# Construct.gs

Construct Grid System – A combination of Taiga's responsive grid and Foundation 4's typography styles.


## Documentation

**Tiaga grid:** http://taigaboilerplate.com/taiga-grid
This is the core grid from, Tiaga. They also offer a "boilerplate" version.


**Foundation 4 – Zurb:** http://foundation.zurb.com
The typographic styles are from, Zurb. As too is the settings file, although this has been modified to accommodate the Tiaga grid.

**Style guide – Paul Robert Lloyd:** http://paulrobertlloyd.com/about/styleguide/
A comprehensive list of mark-up styles, a useful starter for any website.