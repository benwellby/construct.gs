// Except for jQuery and Modernizr prepend all JS files here. The Codekit code below should remain commented out
// @codekit-prepend "vendor/"

$( document ).ready(function() {
  
  
  // svg switch
  if(!Modernizr.svg) {
    $('img[src*="svg"]').attr('src', function() {
      return $(this).attr('src').replace('.svg', '.png');
    });
  }


});